#!/usr/local/bin/python3

'''Script to show Salesforce reports in Chrome browser'''

__version__ = '1.6'
__author__ = 'Nick Heaphy'
__home_url__ = "https://bitbucket.org/nickheaphy/salesforce-automated-report-display"

import pickle
import time
import configuration
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
#from selenium.webdriver.common.keys import Keys


def main():
    '''Main program loop'''

    # Check the version
    print("Checking script version...")
    latest_online_version = return_latest_online_version()
    if latest_online_version is not None and float(latest_online_version) > float(__version__):
        print("Please update script from repository and run again")
        print("You are using %s, while %s is available" % (__version__, latest_online_version))
        print(__home_url__)
        exit()

    # Turn off the infobar and load in fullscreen kiosk mode
    chrome_options = Options()
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument("--kiosk")
    chrome_options.add_argument("--disable-logging")
    chrome_options.add_argument("--force-device-scale-factor=0.75")
    chrome_options.add_argument("--disable-notifications")

    # Open Chrome
    print("Opening Chrome...")
    browser = webdriver.Chrome(chrome_options=chrome_options)

    # open salesforce homepage
    print("Opening Salesforce.com...")
    browser.get('https://www.salesforce.com/')

    # feed the cookie monster
    print("Feeding Cookies to Salesforce.com...")
    try:
        old_cookies = pickle.load(open("salesforce_cookies.pkl", "rb"))
        # we only want to feed with missing cookies
        new_cookies = browser.get_cookies()
        missing_cookies = [cookie for cookie in old_cookies if cookie not in new_cookies]
        # can't set cookies for domains other than what we are on
        for cookie in missing_cookies:
            if cookie['domain'] == ".salesforce.com":
                #print("Setting Cookie: ",cookie)
                browser.add_cookie(cookie)
    except:
        print("  No cookie file found")
    
    print("Opening Reports...")
    open_reports(browser)

    # setup the refresh tracker
    soft_refresh_time_tracker = 0
    hard_refresh_time_tracker = 0

    # Now go into an infinite loop to switch between the tabs
    # not wild about the way I am tracking tabs, but seems to work...
    print("Entering Program Loop...")
    while True:
        # Get an updated list of tabs
        tab_list = browser.window_handles
        for i, tab in enumerate(tab_list):

            # double check that we are still logged in
            if "login" in browser.current_url or "login" in browser.title.lower():
                print("Sorry, need to start script again...")
                open_reports(browser)
                break
                
            # are we due a soft refresh cycle
            if soft_refresh_time_tracker > configuration.soft_refresh_time:
                print("Soft Refreshing Tabs...")
                for tab2 in tab_list:
                    browser.switch_to.window(tab2)
                    refresh_link(browser)
                soft_refresh_time_tracker = 0

            # are we due a hard refresh cycle
            # hard refresh due to seeing problems with soft refresh getting stuck trying to refresh
            if hard_refresh_time_tracker > configuration.hard_refresh_time:
                print("Hard Refreshing Tabs...")
                for j, tab2 in enumerate(tab_list):
                    browser.switch_to.window(tab2)
                    browser.refresh()
                    time.sleep(2)
                    adjust_display(browser,configuration.salesforce_reports[j][2])
                hard_refresh_time_tracker = 0

            # Now display and sleep
            # If something opens and extra tab we could walk off the end of the array
            try:
                browser.switch_to.window(tab)
                sleeptime = configuration.salesforce_reports[i][1]
                soft_refresh_time_tracker += sleeptime
                hard_refresh_time_tracker += sleeptime
                time.sleep(sleeptime)
            except:
                print("Some problem with the tabs")
                print("Sorry, need to start script again...")
                open_reports(browser)
                break
            

            


# -------------------------------------------------------------------
# ######################## Functions ################################
# -------------------------------------------------------------------

def login(browser):
    print("Login Required: Logging into Salesforce...")
    time.sleep(4)
    
    # find the username and password boxes on the login page
    username = browser.find_element_by_id("username")
    password = browser.find_element_by_id("password")

    # fill in the username/password
    username.send_keys(configuration.salesforce_username)
    password.send_keys(configuration.salesforce_password)

    # And click the login box
    browser.find_element_by_name("Login").click()

    # snooze
    time.sleep(4)

    # now check for two factor auth
    if "verification" in browser.current_url:
        print("2 Factor Detected. Please complete login in browser!")
        
        # Wait for verification
        while "verification" in browser.current_url:
            time.sleep(4)
    
    # Save the cookies to bypass 2 factor next time
    save_cookies(browser)


def open_reports(browser):
    # Check how many tabs are open, close all but one
    open_tabs = browser.window_handles
    while len(open_tabs) > 1:
        browser.switch_to.window(open_tabs[1])
        browser.close()
    for i, report in enumerate(configuration.salesforce_reports):
        if i > 0:
            # Open new tab
            browser.execute_script("window.open('about:blank', 'tab%s');" % i)
            browser.switch_to.window("tab%s" % i)
        browser.get(report[0])
        #Lightning seems to take a long time to open
        time.sleep(5)
        if "login" in browser.current_url:
            login(browser)
        adjust_display(browser,report[2])
    

def save_cookies(browser):
    '''Save the cookies from the browsers into a Pickle file'''
    cookie_list_after_login = browser.get_cookies()
    pickle.dump(cookie_list_after_login, open("salesforce_cookies.pkl","wb"))


def adjust_display(browser, zoom=1.00):
    '''User Javascript to change the display to something more appropriate for full screen'''
    time.sleep(1) # Give some time for the page to totally load...
    try:
        # Lightning: .oneHeader .slds-global-header
        browser.execute_script(' var x = document.getElementsByClassName("oneHeader")[0];x[\'style\'] = \'display:none\';')
        browser.execute_script(' var x = document.getElementsByClassName("slds-global-header")[0];x[\'style\'] = \'display:none\';')
        # slds-context-bar
        browser.execute_script(' var x = document.getElementsByClassName("slds-context-bar")[0];x[\'style\'] = \'display:none\';')
        browser.execute_script(' var x = document.getElementsByClassName("stage")[0];x[\'style\'] = \'top:0px\';')
        # hide chat
        #browser.execute_script(' var x = document.getElementById("presence_widget");x[\'style\'] = \'display:none\';')
        # zoom zoom
        # browser.execute_script(' var x = document.getElementsByTagName("BODY")[0];x[\'style\'] = \'transform-origin: top left;transform:scale(%s);\';' % zoom)
        time.sleep(1)
    except:
        print("Something went wrong trying to adjust the display")

def refresh_link(browser):
    '''This uses the embedded refresh option in the Salesforce report to regenerate the report'''
    
    try:
        browser.execute_script('document.getElementsByClassName("refresh")[0].click();')
    except:
        print("Could not refresh")

    

def return_latest_online_version():
    '''Get the latest version number from repository and return'''
    try:
        import urllib.request
        response = urllib.request.urlopen(__home_url__+"/raw/HEAD/README.md")
        data = response.read()
        text = data.decode('utf-8')
        text = text.splitlines()
        for line in text:
            if line[:2] == "v ":
                return line.split(" ")[1]
    except:
        pass
    return None

# -------------------------------------------------------------------
# ######################## End Functions ############################
# -------------------------------------------------------------------


if __name__ == "__main__":
    main()
