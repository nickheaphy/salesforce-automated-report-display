#!/usr/local/bin/python3

# Salesforce login details
salesforce_username = "joe.blogs@somewhere.com"
salesforce_password = "password"

# a list of URLs of the reports, the time to display onscreen (seconds) and the zoom level
# zoom level allows scaling up to better fit screen
salesforce_reports = [  ["https://ap1.salesforce.com/01Z90000000qDnm", 60, "1.10"],
                        ["https://ap1.salesforce.com/01Z90000000qDnh", 20, "1.00"],
                        ["https://ap1.salesforce.com/01Z90000000qDnX", 20, "1.33"]
                    ]

# How much time before reports are soft refreshed (seconds)
soft_refresh_time = 600

# How much time before the tabs are hard refreshed (seconds)
hard_refresh_time = 3600