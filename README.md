# Saleforce Reporting

This quick script uses Selenium to automate the display of Salesforces reports in Chrome and switch between them.

## How it works

Uses Selenium via Python to script Chrome. Logs into Salesforce, accesses the reporting pages, as separate tabs and then cycles through the tabs, periodically refreshing them.

## Setup

Install Chrome on the host computer https://www.google.com/chrome/browser/desktop/index.html

Install Python 3 on the host computer https://www.python.org/downloads/windows/ (ensure you add to PATH)

Install Selenium `python -m pip install selenium`

Download Selenium Webdriver for Firefox and copy to somewhere in the PATH (I copied to C:\Users\username\AppData\Local\Programs\Python\Python36\ as this path is set by the Python installer) https://sites.google.com/a/chromium.org/chromedriver/downloads

Install Pillow `python -m pip install Pillow`

## Installation

Open `configuration-sample.py` and edit with appropriate details. Save as `configuration.py`

Run `loadSalesForceReports.py`

## Version History

v 1.6 - Changes due to moving to Lightning UI in Saleforce

v 1.5 - Adjustments for tabs getting confused.

v 1.4 - Changed some of the logic as something must have changed on the Salesforce side as script was only running for about 24 hrs before something with the tabs changed and the script crashed. Still testing.

v 1.3 - Some bug fixes to get to work on Intel Compute Stick - 12/1/2018

v 1.2 - Fixed bug with hard tab refresh - 12/1/2018

v 1.1 - Added hard tab refresh - 12/1/2018

v 1.0 - Initial Version - 11/1/2018
